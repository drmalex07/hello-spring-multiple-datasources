package helloworld.repository.two;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import helloworld.domain.two.FooEntity;
import helloworld.model.FooDto;

@Repository
@Transactional(readOnly = true)
public interface FooRepository extends JpaRepository<FooEntity, Integer>
{
    default FooDto createFrom(FooDto d)
    {
        Assert.notNull(d, "Expected a DTO object");
        final FooEntity e = new FooEntity(d);
        return this.saveAndFlush(e).toDto();
    }
}
