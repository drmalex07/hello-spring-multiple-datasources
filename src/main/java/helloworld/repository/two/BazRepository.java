package helloworld.repository.two;

import java.time.ZonedDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import helloworld.domain.two.BazEntity;
import helloworld.model.BazDto;

@Repository
@Transactional(readOnly = true)
public interface BazRepository extends JpaRepository<BazEntity, Long>
{
    default BazDto create(ZonedDateTime t)
    {
        BazEntity e = new BazEntity();
        e.setCreatedAt(t);
        e = this.saveAndFlush(e);
        return new BazDto(e.getId(), e.getCreatedAt());
    }
}
