package helloworld.domain.two;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@lombok.ToString
@Entity(name = "Baz")
@Table(name = "`baz`", schema = "public")
public class BazEntity
{
    @lombok.Getter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`id`", updatable = false)
    private Long id;
    
    @lombok.Getter
    @lombok.Setter
    @Column(name = "`created`")
    private ZonedDateTime createdAt;
}
