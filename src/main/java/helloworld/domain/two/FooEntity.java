package helloworld.domain.two;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import helloworld.model.FooDto;

@lombok.ToString
@Entity(name = "Foo")
@Table(name = "`foo`", schema = "public")
public class FooEntity
{
    @lombok.Getter
    @lombok.Setter
    @Id
    @Column(name = "`id`")
    private Integer id;
    
    @lombok.Getter
    @lombok.Setter
    @Column(name = "`name`", nullable = false)
    private String name;
    
    @lombok.Getter
    @lombok.Setter
    @Column(name = "`foo`", nullable = false)
    private Double foo;
    
    public FooDto toDto()
    {
        FooDto x = new FooDto();
        x.setFoo(foo);
        x.setId(id);
        x.setName(name);
        return x;
    }
    
    public FooEntity()
    {}
    
    public FooEntity(FooDto x)
    {
        this.id = x.getId();
        this.name = x.getName();
        this.foo = x.getFoo();
    }
}
