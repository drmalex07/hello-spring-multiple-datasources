package helloworld.domain.one;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import helloworld.model.UserInfo;

@lombok.ToString
@Entity(name = "User")
@Table(name = "`user`", schema = "public")
public class UserEntity
{  
    @lombok.Getter
    @lombok.Setter
    @Id    
    @SequenceGenerator(
        sequenceName = "user_id_seq", name = "user_id_seq", allocationSize = 1)
    @GeneratedValue(
        generator = "user_id_seq", strategy = GenerationType.SEQUENCE)
    @Column(name = "`_id`")
    Long id;
    
    @lombok.Getter
    @lombok.Setter
    @NotNull
    @Column(name = "`name`")
    String name;
    
    @lombok.Getter
    @lombok.Setter
    @Column(name = "`fullname`")
    String fullname;
 
    @lombok.Getter
    @lombok.Setter
    @Column(name = "`password_hash`")
    String password;
    
    @lombok.Getter
    @lombok.Setter
    @Column(name = "`active`")
    Boolean isActive = true;
    
    @lombok.Getter
    @lombok.Setter
    @Column(name = "`blocked`")
    Boolean isBlocked = false;
    
    @lombok.Getter
    @lombok.Setter
    @Column(name = "`registered_at`")
    ZonedDateTime registeredAt;
 
    @lombok.Getter
    @lombok.Setter
    @Column(name = "`token`", nullable = true)
    String token;
    
    @lombok.Getter
    @lombok.Setter
    @Column(name = "`email`", nullable = false)
    String email;
    
    @OneToMany(
        mappedBy = "member", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    List<UserRoleEntity> memberOf = new ArrayList<>();
    
    protected UserEntity() {}
    
    public UserEntity(String name, ZonedDateTime registeredAt) 
    {
        this.name = name;
        this.registeredAt = registeredAt;
    }
    
    public UserEntity(UserInfo userinfo, ZonedDateTime registeredAt) 
    {
        this.name = userinfo.getName();
        this.fullname = userinfo.getFullname();
        this.email = userinfo.getEmail();
        this.registeredAt = registeredAt;
    }
    
    public void addRole(RoleEntity role)
    {
        final Long rid = role.id;
        boolean found = false;
        for (UserRoleEntity p: this.memberOf) {
            if (p.role.id.equals(rid)) {
                found = true;
                break;
            }
        }
        if (!found) {
            this.memberOf.add(UserRoleEntity.newMember(this, role));
        }
    }
    
    public List<RoleEntity> getRoles()
    {
        return memberOf.stream()
            .map(UserRoleEntity::getRole)
            .collect(Collectors.toList());
    }

    /**
     * Return a DTO from this entity
     * @return
     */
    public UserInfo toUserInfo()
    {
        UserInfo u = new UserInfo(id, name, fullname, email);
        u.setRegisteredAt(registeredAt);
        return u;
    }    
}