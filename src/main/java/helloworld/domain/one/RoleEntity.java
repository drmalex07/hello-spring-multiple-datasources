package helloworld.domain.one;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity(name = "Role")
@Table(name = "`role`", schema = "public")
public class RoleEntity
{
    @lombok.Getter
    @lombok.Setter
    @Id
    @SequenceGenerator(
        sequenceName = "role_id_seq", name = "role_id_seq", allocationSize = 1)
    @GeneratedValue(
        generator = "role_id_seq", strategy = GenerationType.SEQUENCE)
    @Column(name = "`_id`")
    Long id;
    
    @lombok.Getter
    @lombok.Setter
    @NotNull
    @Column(name = "`name`")
    String name;
    
    @OneToMany(mappedBy = "role", fetch = FetchType.LAZY)
    Set<UserRoleEntity> members;
    
    public RoleEntity() {}
    
    public RoleEntity(String name) 
    {
        this.name = name;
    }
}
