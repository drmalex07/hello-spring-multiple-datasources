package helloworld.domain.one;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity(name = "UserRole")
@Table(name = "`user_role`", schema = "public")
public class UserRoleEntity
{
    @lombok.Getter
    @lombok.Setter
    @Id
    @SequenceGenerator(
        sequenceName = "user_role_id_seq", name = "user_role_id_seq", allocationSize = 1)
    @GeneratedValue(
        generator = "user_role_id_seq", strategy = GenerationType.SEQUENCE)
    @Column(name = "`_id`")
    Long id;
    
    @lombok.Getter
    @lombok.Setter
    @ManyToOne
    @JoinColumn(name = "`member`", foreignKey = @ForeignKey(name = "fk_user_role_member"))
    UserEntity member;
    
    @lombok.Getter
    @lombok.Setter
    @ManyToOne
    @JoinColumn(name = "`role`", foreignKey = @ForeignKey(name = "fk_user_role_group"))
    RoleEntity role;
    
    public UserRoleEntity() {}
    
    public static UserRoleEntity newMember(UserEntity u, RoleEntity r) 
    {
        UserRoleEntity m = new UserRoleEntity();
        m.role = r;
        m.member = u;
        return m;
    }
}
