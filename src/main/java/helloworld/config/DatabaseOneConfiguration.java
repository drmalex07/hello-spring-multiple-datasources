package helloworld.config;

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import com.zaxxer.hikari.HikariDataSource;

// See https://www.baeldung.com/spring-data-jpa-multiple-databases
// See https://docs.spring.io/spring-boot/docs/2.0.x/reference/html/howto-data-access.html

@Configuration
@EnableJpaRepositories(
    basePackageClasses = { helloworld.repository.one._Marker.class },
    entityManagerFactoryRef = "oneEntityManagerFactory",
    transactionManagerRef = "oneTransactionManager")
public class DatabaseOneConfiguration
{
    @Autowired
    Environment env;
    
    @Bean("oneDataSourceProperties")
    @Primary
    @ConfigurationProperties("helloworld.datasources.one")
    public DataSourceProperties dataSourceProperties() 
    {
        return new DataSourceProperties();
    }

    @Bean("oneDataSource")
    @Primary
    @ConfigurationProperties("helloworld.datasources.one.configuration")
    public HikariDataSource dataSource(
        @Qualifier("oneDataSourceProperties") DataSourceProperties properties) 
    {
        return properties.initializeDataSourceBuilder()
            .type(HikariDataSource.class)
            .build();
    }
    
    @Bean("oneEntityManagerFactory")
    @Primary
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
        @Qualifier("oneDataSource") DataSource dataSource) 
    {
        final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        
        em.setPackagesToScan(new String[] { "helloworld.domain.one" });

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        
        final HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
        properties.put("hibernate.dialect", env.getProperty("hibernate.dialect"));
        properties.put("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
        properties.put("hibernate.format_sql", "true");
        
        em.setJpaPropertyMap(properties);

        return em;
    }
    
    @Bean("oneTransactionManager")
    @Primary
    public PlatformTransactionManager transactionManager(
        @Qualifier("oneEntityManagerFactory") LocalContainerEntityManagerFactoryBean entityManagerFactoryBean) 
    {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactoryBean.getObject());
        return transactionManager;
    }
}
