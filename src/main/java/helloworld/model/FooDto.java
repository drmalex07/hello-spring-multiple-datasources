package helloworld.model;

@lombok.Getter
@lombok.Setter
@lombok.ToString
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class FooDto
{
    private Integer id;
    
    private String name;
    
    private Double foo;
}
