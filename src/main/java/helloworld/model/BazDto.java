package helloworld.model;

import java.time.ZonedDateTime;

@lombok.ToString
@lombok.Getter
@lombok.RequiredArgsConstructor
public class BazDto
{
    private final Long id;
    
    private final ZonedDateTime createdAt;
}
