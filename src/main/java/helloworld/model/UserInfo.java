package helloworld.model;

import java.time.ZonedDateTime;

public class UserInfo
{
    @lombok.Getter
    @lombok.Setter
    private Long id;
    
    @lombok.Getter
    @lombok.Setter
    private String name;
    
    @lombok.Getter
    @lombok.Setter
    private String fullname;
    
    @lombok.Getter
    @lombok.Setter
    private ZonedDateTime registeredAt;
    
    @lombok.Getter
    @lombok.Setter
    private String email;
    
    public UserInfo() {}
    
    public UserInfo(Long id, String name, String fullname, String email)
    {
        this.id = id;
        this.name = name;
        this.fullname = fullname;
        this.email = email;
    }
 
    @Override
    public String toString()
    {
        return String.format("UserInfo [id=%s, name=%s, fullname=%s]", id, name, fullname);
    }
}