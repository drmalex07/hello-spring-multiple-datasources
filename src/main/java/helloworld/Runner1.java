package helloworld;

import java.time.ZonedDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import helloworld.model.BazDto;
import helloworld.repository.one.UserRepository;
import helloworld.repository.two.BazRepository;
import helloworld.repository.two.FooRepository;

@Component
public class Runner1 implements ApplicationRunner
{
    @Autowired
    UserRepository userRepository; // from dataSource "one"
    
    @Autowired
    FooRepository fooRepository; // from dataSource "two"
    
    @Autowired 
    BazRepository bazRepository; // from dataSource "two"
    
    @Override
    public void run(ApplicationArguments args) throws Exception
    {
        //Thread.sleep(10 * 1000l);
        
        System.err.println(userRepository);
        System.err.println(fooRepository);
        System.err.println(bazRepository);
      
        BazDto b1 = bazRepository.create(ZonedDateTime.now());
        System.err.println(b1);
        
        //UserEntity u = userRepository.findByUsername("tester1");
        //System.err.println(u.toUserInfo());
        
        //userRepository.setActive(Arrays.asList(13L, 14L), true);
        
        //fooRepository.createFrom(new FooDto(2, "totos", 42.5));
        
    }
}
