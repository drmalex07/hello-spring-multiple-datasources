CREATE TABLE "foo"
(
    "id" integer PRIMARY KEY,
    "name" varchar(80) NOT NULL,
    "foo" double precision NOT NULL
);


CREATE TABLE "baz"
(
    "id" identity PRIMARY KEY,
    "created" timestamp with time zone NOT NULL 
);
